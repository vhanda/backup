/*
 * This file is part of the KDE Baloo Project
 * Copyright (C) 2014  Vishesh Handa <me@vhanda.in>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QTextStream>
#include <QDebug>

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addPositionalArgument(QLatin1String("subvolume"), QString());
    parser.addOption(QCommandLineOption("r"));
    parser.process(app);

    QStringList args = parser.positionalArguments();
    if (args.size() == 0) {
        return 1;
    }

    QTextStream stream(stdout);
    QStringList commands = parser.positionalArguments();

    QString mainCommand = commands.takeFirst();
    if (mainCommand == QLatin1String("subvolume")) {
        QString subCommand = commands.takeFirst();
        if (subCommand == QLatin1String("list")) {

            QList<QByteArray> paths = qgetenv("KBTRFS_SUBVOLUME_LIST").split(';');
            for (const QByteArray& path : paths) {
                stream << QStringLiteral("ID 1166 gen 428876 top level 5 path ") << path << '\n';
            }
        } else if (subCommand == QLatin1String("snapshot")) {
            bool readOnly = !qgetenv("KBTRFS_SUBVOLUME_SNAPSHOT_READONLY").isEmpty();

            if (readOnly ^ parser.isSet("r"))
                return 1;
            if (commands.size() != 2)
                return 1;

            return 0;
        } else {
            return 1;
        }
    }
    else {
        return 1;
    }

    return 0;
}
