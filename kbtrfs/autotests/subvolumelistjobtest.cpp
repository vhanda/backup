/*
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "subvolumelistjob.h"

#include <QTest>
#include <QSignalSpy>

using namespace KBtrfs;

class SubvolumeListJobTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void init()
    {
        QByteArray path = qgetenv("PATH");
        path = QByteArray(KBTRFS_TEMP_PATH) + ':' + path;

        qputenv("PATH", path);
    }

    void test()
    {
        qputenv("KBTRFS_SUBVOLUME_LIST", QByteArray("subvolume1;subvolume2"));
        SubvolumeListJob job("/");

        QSignalSpy spy(&job, SIGNAL(finished(SubvolumeListJob*)));
        QVERIFY(spy.isValid());
        job.start();
        QVERIFY(spy.wait());

        QCOMPARE(job.subvolumes(), QStringList() << "subvolume1" << "subvolume2");
    }
};

QTEST_MAIN(SubvolumeListJobTest)

#include "subvolumelistjobtest.moc"
