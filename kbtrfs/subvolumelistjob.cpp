/*
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "subvolumelistjob.h"

#include <QStandardPaths>
#include <QProcess>
#include <QDebug>

using namespace KBtrfs;

class SubvolumeListJob::Private {
public:
    QString m_path;
    QProcess m_process;

    QStringList m_subvolumes;
    SubvolumeListJob* q;

    void slotFinished(int exitCode);
};

SubvolumeListJob::SubvolumeListJob(const QString& path)
    : d(new Private)
{
    d->q = this;
    d->m_path = path;
}

SubvolumeListJob::~SubvolumeListJob()
{
    delete d;
}

// don't ask
template<typename... Args> struct SELECT {
    template<typename C, typename R>
    static constexpr auto OVERLOAD_OF( R (C::*pmf)(Args...) ) -> decltype(pmf) {
        return pmf;
    }
};

void SubvolumeListJob::start()
{
    static const QString exePath = QStandardPaths::findExecutable(QStringLiteral("btrfs"));

    d->m_process.setProgram(exePath);
    d->m_process.setArguments({QStringLiteral("subvolume"), QStringLiteral("list"), d->m_path});

    connect(&d->m_process, SELECT<int>::OVERLOAD_OF(&QProcess::finished), [this](int exitCode) {
        d->slotFinished(exitCode);
    });

    d->m_process.start();
}

QStringList SubvolumeListJob::subvolumes() const
{
    return d->m_subvolumes;
}

void SubvolumeListJob::Private::slotFinished(int exitCode)
{
    Q_ASSERT(exitCode == 0);
    QString output = m_process.readAllStandardOutput();
    QStringList lines = output.split('\n');

    m_subvolumes.reserve(lines.size());
    for (const QString& line : lines) {
        int i = line.indexOf(QStringLiteral("path"));
        if (i == -1) {
            continue;
        }

        QString path = line.mid(i + 5).simplified();
        m_subvolumes << path;
    }

    emit q->finished(q);
}
