/*
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "subvolumesnapshotjob.h"

#include <QStandardPaths>
#include <QProcess>
#include <QDebug>

using namespace KBtrfs;

class SubvolumeSnapshotJob::Private {
public:
    QString m_fromPath;
    QString m_toPath;
    SubvolumeSnapshotJob::SnapshotType m_type;
    QProcess m_process;

    bool m_error;
    SubvolumeSnapshotJob* q;

    void slotFinished(int exitCode);
};

SubvolumeSnapshotJob::SubvolumeSnapshotJob(const QString& fromPath, const QString& toPath,
                                           SubvolumeSnapshotJob::SnapshotType type)
    : d(new Private)
{
    d->m_fromPath = fromPath;
    d->m_toPath = toPath;
    d->m_type = type;
    d->m_error = false;
    d->q = this;
}

SubvolumeSnapshotJob::~SubvolumeSnapshotJob()
{
    delete d;
}

// don't ask
template<typename... Args> struct SELECT {
    template<typename C, typename R>
    static constexpr auto OVERLOAD_OF( R (C::*pmf)(Args...) ) -> decltype(pmf) {
        return pmf;
    }
};

void SubvolumeSnapshotJob::start()
{
    static const QString exePath = QStandardPaths::findExecutable(QStringLiteral("btrfs"));

    d->m_process.setProgram(exePath);

    QStringList args = {QStringLiteral("subvolume"), QStringLiteral("snapshot")};
    if (d->m_type == ReadOnly) {
        args << QStringLiteral("-r");
    }
    args << d->m_fromPath;
    args << d->m_toPath;

    d->m_process.setArguments(args);
    connect(&d->m_process, SELECT<int>::OVERLOAD_OF(&QProcess::finished), [this](int exitCode) {
        d->slotFinished(exitCode);
    });

    d->m_process.start();
}


void SubvolumeSnapshotJob::Private::slotFinished(int exitCode)
{
    Q_ASSERT(exitCode == 0);

    QString output = m_process.readAllStandardOutput();
    QStringList lines = output.split('\n');

    qDebug() << output;
    emit q->finished(q);
}

bool SubvolumeSnapshotJob::error() const
{
    // FIXME: Do not always return false
    return false;
}
