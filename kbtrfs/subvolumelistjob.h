/*
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef KBTRFS_SUBVOLUMELISTJOB_H
#define KBTRFS_SUBVOLUMELISTJOB_H

#include <QString>
#include <QObject>
#include "kbtrfs_export.h"

namespace KBtrfs {

class KBTRFS_EXPORT SubvolumeListJob : public QObject
{
    Q_OBJECT
public:
    SubvolumeListJob(const QString& path);
    ~SubvolumeListJob();

    void start();

    // FIXME: This is stupid. It should return a SubVolume object
    QStringList subvolumes() const;

signals:
    void finished(SubvolumeListJob* job);

private:
    class Private;
    Private* d;
};
}

#endif // KBTRFS_SUBVOLUMELISTJOB_H
